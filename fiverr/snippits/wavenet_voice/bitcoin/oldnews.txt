Global COVID-19 total passes 4 million cases
COVID-19 Scan for May 08, 2020
US job losses due to COVID-19 highest since Great Depression
COVID-19 battle echoes smallpox; concerns rise over domestic violence in lockdown
Study finds no hydroxychloroquine effect on death, severe COVID-19
Autopsies of COVID-19 patients reveal clotting concerns
Report: CDC COVID-19 reopening guidelines shelved
COVID-19 surges in Russia, Brazil; WHO warns of huge death toll in Africa
Experts warn COVID-19 lockdowns could have dire impact on TB
Study: Wuhan-like COVID-19 scenario could crush US hospitals

Experts warn COVID-19 lockdowns could have dire impact on TB
A world of misplaced optimism; got gold?
What does the new 'big quest for yield' mean for gold prices next week?
Bitcoin recovery to $10,000 could be just the start of bull run � analysts
Wall St., Main St. expect more gold-price gains in wake of poor U.S. jobs data
Endeavour Silver 1Q revenues decline on mine suspension, build in inventory
Sandstorm Gold's 1Q revenue boosted by higher prices
Endeavour Silver 1Q revenues decline on mine suspension, build in inventory
Sandstorm Gold's 1Q revenue boosted by higher prices
Hecla lists revised 2020 gold, silver output, cost guidance
Kirkland Lake 1Q results boosted by Detour acquisition, higher gold prices
Barrick Gold reports 55% rise in 1Q adjusted profit
Newmont 1Q earnings boosted by higher output, gold prices
Unionized workers strike at Kinross Gold's Tasiast mine
Amplats completes repair to Phase B unit at processing plant
First Cobalt has capacity to supply 5% of global EV cobalt market
Centerra Gold reports 1Q profit, higher metals output
Yamana Gold posts 1Q profit, hikes dividend, trims production guidance
Sprott seizes iron ore project after default
New Gold lists loss for quarter when Rainy River Mine temporarily closed
Silvercorp Metals agrees to acquire Guyana Goldfields
Jacques Perron taking over as CEO of Pretium Resources
Freeport reports 1Q loss, trims copper-output guidance and costs
Teck Resources profit falls short as lockdowns, energy unit bite
Wesdome: 1Q gold output rises; 2Q to be impacted by COVID-19
Barrick Gold: 1Q output on pace to meet 2020 guidance
New gold reports 1Q production, withdraws 2020 guidance
SSR Mining produces 107,000 gold-equivalent ounces in 1Q
Gran Colombia 1Q output impacted by restrictions; limited operations continue
First Majestic 1Q output ahead of suspended guidance
Yamana nets C$201 million for Equinox sale
Jaguar Mining 1Q gold production rises 28% from year ago
Gold prices show little reaction to staggering U.S. job losses
Gold price struggling around $1,720 after 20.5 million jobs reported lost in April
Lucara's capital expansion in doubt as diamond market is thrashed
Balmoral Resources shareholders approve tie up with Wallbridge Mining
TMAC Resources acquired for $149 million
Bitcoin daily chart alert - Prices power to 2.5-mo. high on the "Jones effect" - May 8
'Truly bullish case': Gold price and the 'Japanification of the U.S.' - Pepperstone
Contact Gold CEO, Matthew Lennox-King on the three key areas they want to address for Green Springs, drill results and financing on SFLive
Gold suffering due to unrealistic optimism in equity markets - FXTM
Gold needs to be a core asset for all investors - Buchanan Webinar
Pierre Lassonde relinquishes chair at Franco Nevada
Silvercorp exceeds guidance, 'operations are back to normal'
HSBC sees surge in value-at-risk limit breaches, cites 'delivery disruptions in gold market'
London markets closed Friday; no LBMA auctions
'World's Best Mine Finder' passes at 92
Gold, silver sharply up amid U.S. stock market rally, weaker USDX
Newmont 1Q earnings boosted by higher output, gold prices
ETF gold holdings rise to record high during April
Where is gold headed next week? Vote now!
Gold prices holding below $1,700 as U.S. weekly jobless claims fall to 3.16 million
Commerzbank: Spread between spot gold prices, futures 'normalized' again
Hecla lists revised 2020 gold, silver output, cost guidance
Gold prices up as marketplace focuses on massive U.S. job losses
IPMI Webinar - COVID-19 Challenges and Solutions
Metals Focus: 'conditions look positive for gold miners'
Bitcoin daily chart alert - Bulls in firm command - May 7
Great Bear Resources CEO, Chris Taylor, talks royalty spin-off, timelines, ongoing drilling and more on SFLive
Central-bank gold selling 'limited' but holdings could be used as collateral: Standard Chartered
Nouriel Roubini says global economy faces deadly recession
Wheaton loads up in silver in Q1
Royal Gold reports higher quarterly profit
Franco Nevada warns of 'production curtailments' in Q1
Metals Focus: 'conditions look positive for gold miners'
Pan American Silver hit with $52.7M tax loss and $28.3M investment loss in first quarter
Kirkland Lake 1Q results boosted by Detour acquisition, higher gold prices
Harmony Gold Mining production drops, cash doubles
Gold prices sell off as major refineries reopen, analysts look for spot-futures spread normalization
Gold solidly down amid bearish outside markets: USDX up, oil down
This bubble could decide whether gold goes to $3K or $10K - Celsius Network CEO
Barrick Gold reports 55% rise in 1Q adjusted profit
Twenty-one-year high for sales of platinum eagle coins
Condor Gold receives Nicaragua enviro permits
Gold is going to $2,000 in 12 months - VanEck's Joe Foster
New Resource Lifts Cartier's Chimo Project to the Top
OceanaGold  first-quarter production declines from fourth quarter
Torex Gold Resources reports 1Q adjusted profit
Nighthawk Gold Corp. President & CEO, DR. Michael J. Byron
Rio2 President & CEO Alex Black
Orezone Gold Corp President &  CEO, Patrick Downey
Bitcoin daily chart alert - More upside likely in near term - May 6
Expert panel agrees: 2008 is was nothing in comparison to what is coming.
Endeavour CEO talks about restarting mines in Mexico
Ascot CEO discusses 51% IRR Feasibility Study
Regulus Resources schedules Q&A
Ely Gold Royalties schedules live Q&A
Gold prices weaker following sobering ADP jobs data
Gold price holding support above $1,700 as 20 million private sector jobs lost in April
Gold entering the summer doldrums - Heraeus
Kinross Gold says mines not 'materially impacted' by COVID-19 in Q1
B2Gold touts record revenue, cashflow, gold production
Wesdome Gold Mines ups gold production
ALROSA shutters some operations
Gold weaker as big rally in crude oil boosts U.S. equities
India's gold imports plunge nearly 100% in April on COVID-19 shutdowns, hit multi-decade lows - reports
Danger of second COVID-19 wave could 'kick' gold prices to new record highs - The Perth Mint CEO
Commerzbank: ETF gold demand makes up for drop-off in Indian imports
Gold price holding $1,700 after ISM Non manufacturing Index Falls To more than decade lows
Next step for gold price is to breach the $1,900 peak - Bloomberg Intelligence
Canadian Mining Hall accepting 2021 nominations
Unionized workers strike at Kinross Gold's Tasiast mine
Gold prices down as global risk appetite upticks
Bitcoin daily chart alert - Bullish pennant pattern forms - May 5
Amplats completes repair to Phase B unit at processing plant
World's largest gold miner names biggest COVID-19 operational challenge
Gold is being pushed 'firmly into the $1,700 range'- Newmont
IAMGOLD 2020 production could dip below 700K ounces
Money managers slightly increase bullish positioning in gold, silver
First Cobalt has capacity to supply 5% of global EV cobalt market
Cameco gets a buy rating, trades up 6%
Gold gains as global stock markets fade on U.S.-China strains
Gold price has room to move lower, watch $1,655 - analysts
CME Group: Metals-trading volume rises during April
Discovery Groups live webinar Thursday, May 7th at 11:00 am PST (2:00 pm EST)
RBC's Gero: gold sought as safe haven again; 'look for $1,800'
Diversified miners ranked by effectiveness in reducing greenhouse gas emissions
Roscan trades up 19% on drill results
Great Bear drills 18.57 g/t gold over 13.00 m, within 2.67 g/t gold over 104.15 m at LP fault
Ely Gold announces C$10 million brokered private placement
Marathon Gold announces C$26 million bought deal
Rio Tinto to contribute 10 million to support COVID-19 community initiatives in North America
Cora Gold appoints consultancy for environmental and social impact assessment
Revenues kick in for Eastern Platinum's retreatment project
BBH: Markets to focus on U.S. jobs data, China-U.S. tensions
Gold, silver prices up on safe-haven bids amid U.S.- China tensions
Bitcoin daily chart alert - Normal chart consolidation in an uptrend - May 4
Emerging African miner set to tip production at over 1 million ounces
Will gold's seasonality kick in? Here are the price levels for May
Wall St., Main St. look for bounce in gold prices
Venezuela now handing gold to Iran - more than 9 tons sold already
Gold is an asset you should hold every day - Revival Gold
Does gold listen when central bankers speak?
Petra Diamonds skips interest payment
RJO's Haberkorn: gold prices may ease further but rally to $2,000 by year-end
LBMA: Gold ounces transferred daily hit two-decade high in March
Gold price sees little movement following smaller-than-expected drop in ISM manufacturing data
TDS: 'the balance of risks remains to the upside for gold'
Gold prices weaker even though risk appetite recedes Friday
Pretium Resources reports higher 1Q profit
Bitcoin daily chart alert - Bulls remain in the driver's seat - May 1
Bannockburn: more follow-through gold selling could mean prices test support
Junior investors won't be treated equally when global economy recovers
Venezuela now wants to sell its BOE-stored gold to the U.N. for coronavirus relief - reports
'Inflation bomb' could finally blow up, says J.P. Morgan; what happens next?
Agnico Eagle Mines records Q1 loss
Eldorado production is steady, operates at 75% normal levels
Gold solidly down Thursday, following sell off in U.S. equities
Physical gold demand falls in 1Q; investment may fuel rally to $1,800/oz -  Refinitiv
Gold ETF demand sees 300% annual Increase in first quarter
Gold prices lose some ground as inflation pressures ease
Another 3.84 million Americans file initial U.S. jobless claims; gold remains softer
Gold prices lose overnight gains as U.S. day session begins
Gold prices holding steady up 1% as ECB reaffirmed commitment to supporting European economy
Alacer Gold: 1Q production on line to meet 2020 guidance
Bitcoin daily chart alert - Bulls hit the accelerator this week - Apr. 30
COVID-19 mining shutdowns hurt silver production the most, gold the least - report
Scotiabank's metals business closure could impact daily gold price discovery - analysts
Alamos's gold production down just 12% during COVID-19 quarter
Gold struggles to find direction as Fed sees considerable risks to economic outlook
Gold backs off as risk appetite keener; FOMC on deck
Fresnillo: 1Q silver, gold output in line with expectations
Gold remains softer after pending U.S. home sales fall 20.8% in March
Gold prices sees little reaction after U.S. Q1 GDP falls 4.8%
Osisko pays Taseko C$8.5 million for modified silver stream
Department of Energy launches $9 Million desalination prize
Great Bear receives court approval
Los Filos suspension extended by Mexican gov't
Fresnillo's lower precious metal production is in-line with expectations
Commerzbank: 'expansion of liquidity should continue to boost gold'
Gold prices weaker, show muted reaction to dour U.S. GDP data
The world needs a new kind of currency, says Shanghai Gold Exchange president
Bitcoin daily chart alert - Prices power to 7-week high Wednesday - Apr. 29
Steve Forbes: 'government stupidity' may bring new gold price highs, but gold still 'just a piece of metal'
Cambodia gold miner expects first pour in Q2 2021
Gold trades both sides of unchanged in choppy action Tuesday
Lucapa Diamond reports 'widespread uncertainty', issues securities
'Electrified and partially-automated' mine improves Seabridge's economics
MAG Silver announces C$60 million private placement with Eric Sprott
U.S. dollar is 'the only fly in the ointment' for gold as prices overreach - Mitsubishi
Has the gold seen its highs for the year? World Bank see prices averaging $1,600
CPM Group: pandemic to dent silver supply and demand in 2020
Caterpillar 1Q sales, profit decline from year ago
Gold prices trying to hold on to $1,700 following sharp drop in U.S. Consumer Confidence
OceanaGold resumes mining in New Zealand
Gold prices near steady as global stock markets gain
Catching up with Evergold Corp. (TSXV: EVER)
Bitcoin daily chart alert - Uptrend is the bulls' friend - Apr. 28
Weak Asian gold demand �to test historic lows� but not impact price rally
Headline financings crowd terrible money picture for juniors
Major price dislocations in gold and oil: what do these markets have in common?
Mining industry mourning loss of Peruvian mining legend Guido Del Castillo
Money managers trim bullish positioning in gold futures
Gold sees some normal profit-taking from recent gains
One in six Americans chooses gold as 'best' long-term investment � Gallup
Commerzbank: 'gold should remain in demand as a crisis currency'
Grupo Mexico's mining EBITDA tumbles 23.1% in Q1
Six Fortuna Silver staff test positive for COVID-19
BBH: Federal Reserve to take 'wait-and-see approach'
Bannockburn: New high in gold �looks likely, but it may be marginal�
Gold prices gain as crude oil drops sharply
Bitcoin daily chart alert - Prices power to six-week high, more upside likely - Apr. 27
Train possibly belonging to North Korean leader spotted in resort town: think tank
U.S. economy faces historic shock, with 16% joblessness possible: Trump adviser
Gold price is ready for $1,800: Markets are watching these two events next week
Be prepared for �major correction� in gold price in the coming months says analyst
Investors are turning to the miners as gold, silver prices move higher � CIBC
Wall St., Main St. look for gold prices to strengthen
Silver prices to push towards $20 in 12 months - Bank of America
What is oil telling us about gold prices
Suriname state-owned oil company partners with IAMGOLD
Gold prices up as bulls continue to flex their muscles
Gold prices holding above $1,750 following mixed U.S. durable goods report
Can palladium see temporary surplus this year? Norilsk Nickel makes its projection
'Hold physical gold' not 'synthetic exposures' as prices to challenge all-time highs of $1,920 - UBP
SilverCrest Metals is in "a robust financial position"
Quebec-based Champion Iron ramps up after COVID-19 shutdown
Wesizwe Platinum expects EPS to be 207% higher
Third-party advisors say Balmoral takeover should proceed
SEMAFO founder takes reins at MAYA
Bitcoin daily chart alert - Prices push to six-week high - Apr. 24
Standard Chartered: gold prices supported by investment demand
John Hathaway: if you think gold price will hit new highs, just watch what gold miners will do
Gold bulls continue to push prices higher, helped by oil rebound
Commerzbank sees $18 silver prices by year-end; ETF inflows strong
Sales of new U.S. homes fall 15.4% in March; gold extends gains
Gold prices up 1% as U.S. manufacturing PMI drops to 11-year lows, service sector sees record lows
Initial U.S. jobless claims total 4.43 million; gold holds onto gains
Commerzbank: ETFs post gold inflows 23 days in a row
Bank of America calling for $3,000 gold in 18 months
Bluestone Resources announces C$80 million bought-deal financing
Department of Energy requests $150 million for U.S. uranium reserve
PolyMet attempts to overturn appeals court ruling
Gold, silver prices up as crude oil continues to recover
Metals Focus: risk aversion resulting from weak oil prices supportive for gold
Bitcoin daily chart alert - Recent pause is not bearish - Apr. 23
BBH: U.S. 'stands a decent chance' of ratings downgrade
Hold off on gold price explosion; $2,000 won't come this year, here's why
Investment demand to drive silver price to $19 - World Silver Survey 2020
Three-quarters of Coeur Mining's Q1 revenue derived from gold
Is gold price ready to test $1,800 amid this market 'mayhem'?
Gold prices power higher amid still seriously wounded global economy
Gold prices extend gains after Trump tweet on Iran
Credit Suisse: 'sentiment around gold and gold equities is positive'
Kirkland Lake Gold trades up after announcing corridor of high-grade mineralization
RBC's Gero: $1,800 gold 'not far ahead'
Silver is a 'tagalong' that will underperform gold until this catalyst kicks in - Scotiabank
Virtual Metals Investor Forum on April 30th
Conic Metals on how to operate during the COVID-19 crisis
Westhaven Ventures takes Q&A on its Shovelnose gold project
Gold prices sharply up as U.S. stocks, crude oil rebound
Bitcoin daily chart alert - Bulls hanging tough at mid-week - Apr, 22
Gold's bull-run is hitting end of the runway; year-end target is $1,600 - Capital Economics
Confusion over North Korea's Kim Jong Un being seriously ill shakes markets, gold price
Time to target $1,900 gold price, here's the timeline - TD Securities
Commerzbank: ETFs still accumulating gold, silver
Sales of U.S. existing homes fall 8.5% in March; gold remains softer
Gold down as raw commodity sector shudders amid crude oil collapse
Rob Henderson named Great Panther CEO; David Garofalo board chair
Hold gold as exit from COVID-19 stimulus measures will be messy - CIBC
Centamin maintains 2020 gold-output guidance after in-line 1Q production
Gold, silver prices sharply down as buyers spooked by crude oil
Crouching silver, hidden oil market report 20 Apr.
BMO: traders get lesson on rolling futures positions
Bitcoin daily chart alert - Bulls maintaining price uptrend - Apr. 21
Historic oil crash; buying these gold stocks is perfect way to profit - Haywood Securities
'Safe-haven rules all': ETF demand to drive gold price, but physical market to suffer - BMO
Gold needn't get any better to make investors a lot of money right now: Keech
United States Mint to resume operations at West Point
What do massive losses in oil mean for the gold price?
Unfathomable drop in Nymex crude oil prices suggests futures market is broken
Pebble chalks up court win
Safe-haven demand boosts gold, silver, as crude oil crashes
Denver Gold's virtual mining conference is an industry first
Long-term investors are the ones boosting gold, not futures traders - analysts
FXTM: 'disconnect' between economy, stocks; long-term key to be virus trajectory
NASA wants to make oxygen on Mars and gold is a key part of it
Shanta Gold produces 20K ounces in Q1
Trilogy Metals snags former Ivanhoe exec
Gold prices recover overnight losses as crude oil craters
Bitcoin daily chart alert - Bulls in control, bigger move coming - Apr. 20
Russians withdraw record amount of rubles as COVID-19 worries mount: report
Gold market should evolve from latest crisis - Tradewind Markets CEO
Gold price to end the year at $1,700; physical demand to remain strong � ABN AMRO
Dramatic shift: Why gold prices might not breach $1,800 this spring
Vale's iron ore production off 18% in Q1; worse may lie ahead
Lower oil prices to decimate EV sales
Gold prices to target all-time highs but don't expect silver to follow - Refinitiv
Wall Street still bullish on gold prices but tempers enthusiasm
Some light in the darkness
Gold, silver prices down as risk appetite up late this week
Gold price breaks $1,700; let it ride to $2,000 - Bank of America
Higher gold prices to trigger spike in M&A activity in 2020 - Fitch Solutions
Bitcoin daily chart alert - Recent sideways trading not bearish - Apr. 17
'Bottom in silver': 2020 is 'the best buying opportunity' - ABC Bullion
Underground copper mine in Montana has all required permitting
Gold lower on some profit taking from recent gains and higher USDX
Another 5.25 million Americans file U.S. initial jobless claims; gold prices remain higher
TDS: gold stronger, market prices in 'unprecedented stimulus'
Commerzbank: Temporary U.S. Mint shutdown could mean supply shortage of gold coins
B2Gold: exploration-camp worker tests positive for COVID-19; Fekola mine still operating
COVID-19 risks shut down U.S. Mint gold silver bullion production at West Point
Gold prices solidly up as bulls again hit the accelerator
U.S. housing construction dropped 22% in March
Thailand PM to the people after rush to sell gold: �Sell gradually�
Philly Fed Survey shows sentiment in manufacturing sector at 40-year low
Gold price tracking - could be $10k before we win �WW3� says Frank Holmes
Bitcoin daily chart alert - Bulls keeping price uptrend alive - Apr. 16
Commerzbank sees $1,800 gold prices as investors seek 'last-resort lifeline'
New York Fed reports historic drop in Empire State Manufacturing Survey
Royal Canadian Mint joins fight against COVID-19 by making hand sanitizer, face shields
Gold, silver see big pullbacks after recent bigger gains; not surprising
Spread in gold market continues to baffle investors as futures prices eye higher levels
Roxgold jumps 12% on PEA
Gold price sees further technical selling following 8.7% decline in U.S. retail sales
BBH: dollar 'fundamentally attractive' compared to major currencies
World's first all-electric open-pit mine is on the drawing board
About half of world's production of uranium has been cut - Haywood
Gold, silver prices see normal corrective pullbacks from recent gains
Cracking the work from home code, this couple built a business empire out of it
Bitcoin daily chart alert - Recent pause is not bearish - Apr. 15
Commerzbank: further stimulus would boost gold as 'store of value'
As IMF warns of 'crisis like no other', gold is working its way to new record highs
Covid-19 is 'nearest thing to a world war,' this is what to buy
Tactical reasons to buy silver, but gold continues to outperform - UBS
'This is the time when you should own gold' - Dennis Gartman
ETF gold, silver holdings rise sharply in April along with prices
Quebec gold mines deemed essential service, to reopen Wednesday
Gold, silver bulls bask amid bullish charts, safe-haven buying
Western Australia rare earth miner receives enviro permits
'We are in totally unprecedented territory' - Nolan Watson
Glencore re-opens Raglan mine in Quebec
Torex Gold on pace to meet guidance before COVID-19 suspension
McEwen Mining restarting Black Fox operations in Ontario
A gold price rally to $2,000 would not be a surprise - Sprott's Grosskopf
Gold prices could see a more bullish run than 2009-2012, but there is a caveat, says Scotiabank
Gold futures prices at 7.5-yr. high on safe-haven demand, bullish charts
FXTM: gold hits seven-year high; more gains if dollar eases further
Bitcoin daily chart alert - Chart consolidation early this week - Apr. 14
'Gold is a hedge against the madness': Price pullbacks are now buying opportunities - Pepperstone
Mandalay Resources reports higher first-quarter production
Gold is back: why the yellow metal will continue to rule the safe haven domain
Gold is the only commodity that has had investor in-flows year-to-date - Citi
Analysts look for funds to start building bullish futures positioning in gold
Gold bulls flex their muscles; futures prices hit 7.5-yr. high
Antimina temporarily suspended
Cameco extends closures, withdraws outlook
Chinese, Indian gold jewelry demand falling off a cliff - Capital Economics
BBH: key U.S. economic data to be retail sales, jobless claims
SilverCrest Metals announces another C$75 million financing
Bitcoin daily chart alert - Normal downside correction Monday - Apr. 13
Barrick Gold signs earn-in agreement with Gold Minerals
ValOre Metals Video Angilak Uranium April 9, 2020
Gold prices see normal corrective pullback amid strong uptrend
Risk Rally Inconclusive, Dollar Index and Gold Range Bound
CFTC Commitments of Traders � Traders Trimmed Bets on USD Index Futures amidst Economic Uncertainty
CFTC Commitments of Traders � NET LENGTH in Crude Oil Futures Declined after Weeks of Price Recovery
Markets Shrug Record Labor Down Turn, Dollar Soft With Yen on Risk Sentiments
What to Expect from CCP�s Two Sessions?
RBA Forecasts Double-Digit Contraction and Unemployment in First Half of the Year
Dollar Softens on Negative Rate Talks, US-China Trade Deal Made Progress
Yen Dips on Higher Stocks and Oil, Dollar Shrugs Job Numbers
Sterling Recovers after BoE, Yen Paring Gains
Crude Oil Price Recovers as US Stockbuild Slowed and OPEC+ Began Output Cut
Dollar Firm Despite Historic ADP Job Loss, But Yen Stronger
Yen Surges Through Resistance Against Dollar and Euro
Euro Tumbles as German Court Judges ECB Bond Buying Partially Contravenes the Law
Aussie Follows Stocks Higher, Steady after RBA
Euro Weighed Down by Sentiment Indicators, Markets in Mild Risk-Off
BOE to Keep Stimulus Measures Unchanged in May. Could Announce QE Extension in Coming Two Months
Markets in Defense on US-China Tensions, Dollar and Yen Higher
RBA to Stay on Hold in May. Economic Forecasts to be Downgraded
From Coronavirus Lockdown Exit to New US-China Tariff war
