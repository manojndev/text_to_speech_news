#!/usr/bin/env python3
import os
import sys

# speak stuff
speak_rate = 1.0



###############################################################parsing the xml#############################################

import bs4 as bs
import urllib.request
import xml.etree.ElementTree as ET



source = urllib.request.urlopen('http://news.kitco.com/rss/').read()
soup = bs.BeautifulSoup(source,'xml')





########################################################################################################################################################
class news:
  def __init__(self, title,content,url):
    self.title = title
    self.content = content
    self.url=url






newsobjarr=[]




############################################################################################################################################################




############################################################################scraping the data from the website########################################################

def getnewscontent(link):
  import requests
  from bs4 import BeautifulSoup

  url = link
  r = requests.get(url)
  soup = BeautifulSoup(r.text, "html.parser")
  div = soup.find('article', {'itemprop': 'articleBody'})


  for s in div.select('script'):
    s.extract()
  #print(div.get_text())
  return  div.get_text()









#############################################################################scraping the data form the web site####################################################


#############################################################################uploading file to the anchor ##########################################################

def uploadfiletoanchor(titles):

  from selenium import webdriver
  from selenium.webdriver.common.keys import Keys
  import os
  driver = webdriver.Chrome()

  import time

  driver.get("https://anchor.fm/login")
    
  emailid=driver.find_element_by_id("email")
  emailid.send_keys("Goldaucast@gmail.com")

  password=driver.find_element_by_id("password")
  password.send_keys("texttospeech")
  password.send_keys(Keys.RETURN)
  time.sleep(3)
  driver.get("https://anchor.fm/dashboard/episode/new")
  driver.find_element_by_xpath("/html/body/div/div/div/div/div/div/div/div[2]/div[1]/div[1]/div/input").send_keys(os.getcwd()+"/0.mp3")
  time.sleep(60)
  driver.find_element_by_xpath("/html/body/div/div/div/div/div/div/div/div[2]/button").click()

  time.sleep(5)

  title=driver.find_element_by_id("title")

  title.send_keys(titles)


  body=driver.find_element_by_xpath("/html/body/div/div/div/div/div/form/div[1]/div[2]/div[2]/div/label/div[2]/div/div/div[2]/div/div[2]/div")
  body.send_keys(titles)
  time.sleep(2)

  driver.find_element_by_xpath("/html/body/div/div/div/div/div/form/div[1]/div[1]/div[2]/div[2]/div/div/div/button").click()

  time.sleep(3)
  driver.close()





###########################################################################uploading file to the anchor#########################################################




















f= open("oildata.xml","w+")
f.write(str((soup).encode("utf-8")))




root = ET.fromstring(str(soup))

newsstr=""

title=[]
link=[]

for child in root:
  #print(child.tag, child.attrib)
  if(child.tag=="channel"):
    for c in child:
      #print(c.tag)
      if(c.tag=="item"):
        for data in c:
          
          #print(data.tag ,data.text)
          if(data.tag=="title"):
            #newsstr=newsstr+data.text+"."                                        ###########################this is the new str
            title.append(data.text)
            
          if(data.tag=="link"):
           link.append(data.text)
        
            

          

          # newsobj=news(title,"",link)
          # if not link:
          #   kanji="manji"
          # else:
          #   newsobjarr.append(newsobj)


          
            #newsobj=news()


####################################################sving the array to an object##########################################################################
countsave=0
for i in range(0,len(title)):
  newsobjarr.append(news(title[i],"",link[i]))



######################################################saving an array to an object#######################################################################



          





#############################################################################################filtering out the old news and saving the new news#########################################################
publishnewsarr=[]

# from gtts import gTTS

# tts = gTTS(text=newsstr, lang='en')
# tts.save('file.mp3')

oldnewsarr=[]
file1 = open('oldnews.txt', 'r') 
Lines = file1.readlines() 
for line in Lines:
  #print(line.strip())
  if line.rstrip():
    oldnewsarr.append(line.strip())
  
count=0
for new in newsobjarr:
  count=0
  for old in oldnewsarr:
    if(old.strip()==new.title.strip()):
      count=count+1
  if(count==0):
    print(new.title)
 
    ############################################################appending to the old news file for initial saving#############################################################
    # oldnewsfile= open("oldnews.txt","a+")
    # oldnewsfile.write("\n"+new.title) 


    #############################################################appending to the old news file for the initial saving#######################################################

    print(new.url)
    print(getnewscontent(new.url))
    #newsstr=newsstr+new.title+"."+getnewscontent(new.url).replace('\r', '').replace('\n', '')
    data=getnewscontent(new.url).replace('\r', '').replace('\n', '')
    data=data.strip()
    data=data.replace('stressed-out_doc.jpg', '')
    data=data.replace('Halfpoint / iStock', '')
    data=data.replace('studio9 / iStock', '')

    info = (data[:4900] + '..') if len(data) > 4999 else data
    publishnewsarr.append(news(new.title,info,new.url))
      
    






print(len(oldnewsarr))
print(len(newsobjarr))










#############################################################################################filtering out the old news and saving the new news##############################################################





































for publish in publishnewsarr:
  newsstr=""
  newsstr=newsstr+publish.title+publish.content
  f= open("text.txt","w+")
  f.write(newsstr) 




  def synthesize_text(text, client):
    """Synthesizes speech from the input string of text."""

    # input format
    input_text = texttospeech.types.SynthesisInput(text=text)

    # Note: the voice can also be specified by name.
    # Names of voices can be retrieved with client.list_voices().
    voice = texttospeech.types.VoiceSelectionParams(
        language_code='en-US',
        ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE)

    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3,
        speaking_rate=speak_rate)

    response = client.synthesize_speech(input_text, voice, audio_config)

    return response.audio_content


  def write_to_file(audio, name):
    # The response's audio_content is binary.
    with open(str(name) + '.mp3', 'wb') as out:
      print('Audio content written to file', name)
      out.write(audio)


  if __name__ == '__main__':

    # get the credentials
    pwd = os.getcwd()
    credential = ""
    for f in os.listdir("./credentials/"):
      if ".json" in f:
        credential = f
    if credential == "":
      print("No credentials json found in 'credentials' dir")
      quit()
    else:
      credential = os.path.join(pwd, "credentials", credential)
      print("Found credential file", credential)
      os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential
      from google.cloud import texttospeech
      client = texttospeech.TextToSpeechClient()

    # open text file as a list of lines
    with open("text.txt") as f:
      content = f.readlines()


    # remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]

    # remove empty lines
    content = [x for x in content if len(x) > 0]

    # download every line of text into a numbered mp3
    for i, line in enumerate(content):
      audio = synthesize_text(line, client)
      write_to_file(audio, str(i))
    uploadfiletoanchor(publish.title)

    oldnewsfile= open("oldnews.txt","a+")

    oldnewsfile.write("\n"+publish.title) 
      
  f= open("text.txt","a+")
  f.write(newsstr)  